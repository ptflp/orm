package orm

import (
	"go.uber.org/zap"

	"gitlab.com/golight/orm/db"
	"gitlab.com/golight/orm/db/adapter"
	"gitlab.com/golight/orm/infrastructure/db/migrate"

	"gitlab.com/golight/orm/utils"
)

func NewOrm(dbConf utils.DB, scanner utils.Scanner, logger *zap.Logger) (*adapter.SQLAdapter, error) {
	var (
		orm *db.SqlDB
		err error
	)

	orm, err = db.NewSqlDB(dbConf, scanner, logger)
	if err != nil {
		logger.Fatal("error init db", zap.Error(err))
	}
	migrator := migrate.NewMigrator(orm.DB, dbConf, scanner)
	err = migrator.Migrate()
	if err != nil {
		logger.Fatal("migrator err", zap.Error(err))
	}
	return orm.SqlAdapter, nil
}
