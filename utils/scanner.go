package utils

import "gitlab.com/golight/orm/infrastructure/db/scanner"

const (
	Update = "update"
	Create = "create"
)

func NewTableScanner() Scanner {
	return &scanner.TableScanner{}
}

type Scanner interface {
	RegisterTable(entities ...scanner.Tabler)
	OperationFields(tableName, operation string) []string
	Table(tableName string) scanner.Table
	Tables() map[string]scanner.Table
}

type Tabler interface {
	TableName() string
	OnCreate() []string
}
